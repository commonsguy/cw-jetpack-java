/*
  Copyright (c) 2018-2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.contact;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;
import com.commonsware.jetpack.samplerj.contact.databinding.ActivityMainBinding;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends AppCompatActivity {
  private ContactViewModel vm;
  private ActivityMainBinding binding;

  private final ActivityResultLauncher<Void> pickContact =
    registerForActivityResult(new ActivityResultContracts.PickContact(),
      new ActivityResultCallback<Uri>() {
        @Override
        public void onActivityResult(Uri uri) {
          vm.setContact(uri);
          updateViewButton();
        }
      });

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    binding = ActivityMainBinding.inflate(getLayoutInflater());
    setContentView(binding.getRoot());

    vm = new ViewModelProvider(this).get(ContactViewModel.class);

    updateViewButton();

    binding.pick.setOnClickListener(v -> pickContact.launch(null));

    binding.view.setOnClickListener(
      v -> {
        try {
          startActivity(new Intent(Intent.ACTION_VIEW, vm.getContact()));
        }
        catch (Exception e) {
          Toast.makeText(this, R.string.msg_view_error,
            Toast.LENGTH_LONG).show();
        }
      });
  }

  private void updateViewButton() {
    if (vm.getContact() != null) {
      binding.view.setEnabled(true);
    }
  }
}
