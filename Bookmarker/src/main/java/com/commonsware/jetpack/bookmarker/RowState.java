/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker;

import android.text.Spanned;
import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.DiffUtil;

public class RowState {
  public final Spanned title;
  public final String iconUrl;
  final String pageUrl;

  RowState(BookmarkModel model) {
    this.title =
      HtmlCompat.fromHtml(model.title, HtmlCompat.FROM_HTML_MODE_COMPACT);
    this.iconUrl = model.iconUrl;
    this.pageUrl = model.pageUrl;
  }

  final static DiffUtil.ItemCallback<RowState> DIFFER =
    new DiffUtil.ItemCallback<RowState>() {
      @Override
      public boolean areItemsTheSame(@NonNull RowState oldItem,
                                     @NonNull RowState newItem) {
        return oldItem == newItem;
      }

      @Override
      public boolean areContentsTheSame(@NonNull RowState oldItem,
                                        @NonNull RowState newItem) {
        return oldItem.title.toString().equals(newItem.title.toString());
      }
    };
}
