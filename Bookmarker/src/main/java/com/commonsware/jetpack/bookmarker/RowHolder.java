/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.bookmarker;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;
import com.commonsware.jetpack.bookmarker.databinding.RowBinding;
import androidx.recyclerview.widget.RecyclerView;

class RowHolder extends RecyclerView.ViewHolder {
  final private RowBinding binding;

  RowHolder(RowBinding binding) {
    super(binding.getRoot());

    this.binding = binding;
  }

  void bind(RowState state) {
    binding.setState(state);
    binding.executePendingBindings();

    binding.getRoot().setOnClickListener(view -> {
      Intent viewPage =
        new Intent(Intent.ACTION_VIEW, Uri.parse(binding.getState().pageUrl));

      try {
        binding.getRoot().getContext().startActivity(viewPage);
      }
      catch (ActivityNotFoundException ex) {
        Toast.makeText(
          binding.getRoot().getContext(),
          "Sorry, we cannot view that URL!",
          Toast.LENGTH_LONG
        ).show();
      }
    });
  }
}
