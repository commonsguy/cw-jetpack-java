package com.commonsware.jetpack.bookmarker;

import android.content.Context;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

class BookmarkRepository {
  private static volatile BookmarkRepository INSTANCE;
  private final BookmarkDatabase db;
  private final Executor executor = Executors.newSingleThreadExecutor();

  synchronized static BookmarkRepository get(Context context) {
    if (INSTANCE == null) {
      INSTANCE = new BookmarkRepository(context.getApplicationContext());
    }

    return INSTANCE;
  }

  private BookmarkRepository(Context context) {
    db = BookmarkDatabase.get(context);
  }

  LiveData<BookmarkResult> save(String pageUrl) {
    return new SaveLiveData(pageUrl, executor, db.bookmarkStore());
  }

  LiveData<List<BookmarkModel>> load() {
    return Transformations.map(db.bookmarkStore().all(),
      entities -> {
        ArrayList<BookmarkModel> result = new ArrayList<>();

        for (BookmarkEntity entity : entities) {
          result.add(new BookmarkModel(entity));
        }

        return result;
      });
  }

  private static class SaveLiveData extends LiveData<BookmarkResult> {
    private final String pageUrl;
    private final Executor executor;
    private final BookmarkStore store;

    SaveLiveData(String pageUrl, Executor executor, BookmarkStore store) {
      this.pageUrl = pageUrl;
      this.executor = executor;
      this.store = store;
    }

    @Override
    protected void onActive() {
      super.onActive();

      executor.execute(() -> {
        try {
          BookmarkEntity entity = new BookmarkEntity();
          Document doc = Jsoup.connect(pageUrl).get();

          entity.pageUrl = pageUrl;
          entity.title = doc.title();

          // based on https://www.mkyong.com/java/jsoup-get-favicon-from-html-page/

          String iconUrl = null;
          Element candidate = doc.head().select("link[href~=.*\\.(ico|png)]").first();

          if (candidate == null) {
            candidate = doc.head().select("meta[itemprop=image]").first();

            if (candidate != null) {
              iconUrl = candidate.attr("content");
            }
          }
          else {
            iconUrl = candidate.attr("href");
          }

          if (iconUrl != null) {
            URI uri = new URI(pageUrl);

            entity.iconUrl = uri.resolve(iconUrl).toString();
          }

          store.save(entity);

          postValue(new BookmarkResult(new BookmarkModel(entity), null));
        }
        catch (Throwable t) {
          postValue(new BookmarkResult(null, t));
        }
      });
    }
  }
}
