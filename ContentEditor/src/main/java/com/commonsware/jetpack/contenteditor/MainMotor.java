/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor;

import android.app.Application;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

public class MainMotor extends AndroidViewModel {
  private final TextRepository repo;
  private final MediatorLiveData<StreamResult> results = new MediatorLiveData<>();
  private LiveData<StreamResult> lastResult;

  public MainMotor(@NonNull Application application) {
    super(application);

    repo = TextRepository.get(application);
  }

  MediatorLiveData<StreamResult> getResults() {
    return results;
  }

  void read(Uri source) {
    if (lastResult != null) {
      results.removeSource(lastResult);
    }

    lastResult = repo.read(source);
    results.addSource(lastResult, results::postValue);
  }

  void write(Uri source, String text) {
    if (lastResult != null) {
      results.removeSource(lastResult);
    }

    lastResult = repo.write(source, text);
    results.addSource(lastResult, results::postValue);
  }
}
