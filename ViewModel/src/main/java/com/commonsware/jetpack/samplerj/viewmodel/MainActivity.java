/*
  Copyright (c) 2018 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.viewmodel;

import android.os.Bundle;
import android.util.Log;
import com.commonsware.jetpack.samplerj.viewmodel.databinding.ActivityMainBinding;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

public class MainActivity extends AppCompatActivity {
  private static final String TAG = "ViewModel";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    final ActivityMainBinding binding =
      ActivityMainBinding.inflate(getLayoutInflater());

    setContentView(binding.getRoot());

    ColorAdapter adapter = new ColorAdapter(getLayoutInflater());
    ColorViewModel vm = new ViewModelProvider(this).get(ColorViewModel.class);

    adapter.submitList(vm.numbers);
    binding.items.setLayoutManager(new LinearLayoutManager(this));
    binding.items.addItemDecoration(
      new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    binding.items.setAdapter(adapter);

    Log.d(TAG, "onCreate() called!");
  }

  @Override
  protected void onStart() {
    super.onStart();

    Log.d(TAG, "onStart() called!");
  }

  @Override
  protected void onResume() {
    super.onResume();

    Log.d(TAG, "onResume() called!");
  }

  @Override
  protected void onPause() {
    Log.d(TAG, "onPause() called!");

    super.onPause();
  }

  @Override
  protected void onStop() {
    Log.d(TAG, "onStop() called!");

    super.onStop();
  }

  @Override
  protected void onDestroy() {
    Log.d(TAG, "onDestroy() called!");

    super.onDestroy();
  }
}
