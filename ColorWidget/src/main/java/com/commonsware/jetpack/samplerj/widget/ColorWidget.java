/*
  Copyright (c) 2021 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.samplerj.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import java.util.Random;
import androidx.navigation.NavDeepLinkBuilder;

public class ColorWidget extends AppWidgetProvider {
  private static final String ACTION_REFRESH = "refresh";
  private static final String EXTRA_APP_WIDGET_ID = "appWidgetId";

  private final Random random = new Random();

  @Override
  public void onReceive(Context context, Intent intent) {
    if (intent.getAction().equals(ACTION_REFRESH) &&
      intent.hasExtra(EXTRA_APP_WIDGET_ID)) {
      AppWidgetManager appWidgetManager =
        context.getSystemService(AppWidgetManager.class);

      updateWidget(
        context,
        appWidgetManager,
        intent.getIntExtra(EXTRA_APP_WIDGET_ID, -1)
      );
    }
    else {
      super.onReceive(context, intent);
    }
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                       int[] appWidgetIds) {
    for (int appWidgetId : appWidgetIds) {
      updateWidget(context, appWidgetManager, appWidgetId);
    }
  }

  private void updateWidget(Context context, AppWidgetManager appWidgetManager,
                            int appWidgetId) {
    RemoteViews remoteViews =
      new RemoteViews(context.getPackageName(), R.layout.widget);
    int color = random.nextInt();

    remoteViews.setTextViewText(
      R.id.label,
      context.getString(R.string.label_template, color)
    );
    remoteViews.setInt(R.id.root, "setBackgroundColor", color);

    Intent refreshIntent = new Intent(context, ColorWidget.class)
      .setAction(ACTION_REFRESH)
      .putExtra(EXTRA_APP_WIDGET_ID, appWidgetId);
    PendingIntent refreshPI = PendingIntent.getBroadcast(
      context,
      appWidgetId,
      refreshIntent,
      PendingIntent.FLAG_UPDATE_CURRENT
    );

    remoteViews.setOnClickPendingIntent(R.id.refresh, refreshPI);

    Bundle args = new Bundle();

    args.putInt("color", color);

    PendingIntent deepLinkPI = new NavDeepLinkBuilder(context)
      .setGraph(R.navigation.nav_graph)
      .setDestination(R.id.bigSwatchFragment)
      .setArguments(args)
      .createPendingIntent();

    remoteViews.setOnClickPendingIntent(R.id.root, deepLinkPI);

    appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
  }
}
